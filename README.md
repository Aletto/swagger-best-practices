> 使用Swagger2可以比较方便的生成API文档，并测试接口，但是体验相比Postman差太多且无法保存，好在可以将swagger数据导入到Postman,下面将演示如何将导入

# 将Swagger数据导入到Postman
项目运行后，可以直接访问```/v2/api-docs```路径，我们只需要将这个路径导入Postman，就可以生成Collections，如下图所示:

![导入到Postman](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/45c3441bf57b4ec9a52a644ff2b71755~tplv-k3u1fbpfcp-watermark.image)

***Swagger2与Postman结合使用的关键在于指定正确的```paramType```***,其提供了五种类型:```form``` ```query``` ```body``` ```path``` ```header``` 只有指定了正确的类型，导入到Postman时其才能正确识别将值设置在何处，更多例子文章底部的项目中，如下是指定为```path```时生成的接口

![path参数](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/239af96b8e4d428fb88ac276d8c98cec~tplv-k3u1fbpfcp-watermark.image)

# 问题
Swagger2导入后会自动生成```baseUrl```变量，变量值为```//host:port/```我们需要对其更改，否则最终请求时会变成```http:////host:port/```导致请求失败, 点击***Collection -> Edit -> Variables***进行修改!

![修改环境变量](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/1896f8723cb64228abc28bbdfcde8952~tplv-k3u1fbpfcp-watermark.image)

# 其他
相关代码在[Swagger2最佳实践](https://gitee.com/Aletto/swagger-best-practices)
