package com.example.demo.controllers;

import com.example.demo.pojo.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
@Api(tags = "订单")
public class OrderController {


    @PutMapping("/cancel/{id}")
    @ApiOperation(value = "取消订单", notes = "参数类型为path，用于导入postman")
    @ApiImplicitParam(name = "id", value = "订单ID", paramType = "path", required = true, dataType = "Long")
    public ResponseEntity<Object> cancel(@PathVariable(name = "id") Long id) {

        return ResponseEntity.success(id);
    }


    @PutMapping("/add/{id}")
    @ApiOperation(value = "添加订单")
    @ApiImplicitParam(name = "id", value = "订单ID", paramType = "path", required = true, dataType = "Long")
    public ResponseEntity<Object> add(@PathVariable(name = "id") Long id) {

        return ResponseEntity.success(id);
    }


}
