package com.example.demo.pojo;

public enum ErrorCodeEnum {

    SUCCESS(200, "成功"),
    UNAUTHORIZED(401, "未授权"),
    FORBIDDEN(403, "访问受限，授权过期"),
    NOT_FOUND(404, "资源，服务未找到");

    private Integer Code;
    private String  Msg;

    ErrorCodeEnum(Integer Code, String Msg) {
        this.Code = Code;
        this.Msg = Msg;
    }

    public Integer getCode() {
        return Code;
    }

    public String getMsg() {
        return Msg;
    }

}
