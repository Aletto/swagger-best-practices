package com.example.demo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "登录请求参数")
public class LoginParam {

    @ApiModelProperty(notes = "手机码", example = "13027169611")
    private String mobile;

    @ApiModelProperty(notes = "验证码", example = "123456")
    private String code;


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
