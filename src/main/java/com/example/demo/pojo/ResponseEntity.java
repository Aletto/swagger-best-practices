package com.example.demo.pojo;

import io.swagger.annotations.ApiModelProperty;

public class ResponseEntity<T> {

    public static ResponseEntity<Object> success() {

        return new ResponseEntity<>(ErrorCodeEnum.SUCCESS.getCode(), "成功", null);
    }

    public static <T> ResponseEntity<T> success(T data) {

        return new ResponseEntity<>(ErrorCodeEnum.SUCCESS.getCode(), "成功", data);
    }

    public static ResponseEntity<Object> error(String msg) {

        return new ResponseEntity<>(ErrorCodeEnum.NOT_FOUND.getCode(), msg, null);
    }


    @ApiModelProperty(notes = "响应码", example = "200")
    private int code;

    @ApiModelProperty(notes = "响应消息", example = "成功")
    private String msg;

    private T data;

    private ResponseEntity(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
