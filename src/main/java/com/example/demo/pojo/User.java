package com.example.demo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "用户模型")
public class User  {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(notes = "用户名",  example = "张三")
    private String name;

    @ApiModelProperty(notes = "性别",  example = "1")
    private Integer sex;

    @ApiModelProperty(notes = "年龄", name = "age", example = "18")
    private Integer age;


    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}
